import os
import json
import shutil


class AdminMaker:
    def __init__(self, args):
        # check admin dir
        self.admin_root = args.admin_dir
        if not (os.path.exists(self.admin_root) and os.path.isdir(self.admin_root)):
            print("cannot find admin project")
            exit(13)

        self.admin_repo_root = os.path.join(
            self.admin_root,
            "src/main/kotlin/ai/maum/api/admin/infra/repository"
        )

        self.name = args.name
        self.force = args.force
        self.integrate = args.integrate

        # check admin repository dir
        if (not (os.path.exists(self.admin_repo_root)
                 and os.path.isdir(self.admin_repo_root)) or
                ((os.path.exists(os.path.join(
                    self.admin_repo_root,
                    "{}ModelRepository.kt".format(str(self.name).capitalize()))
                ) or
                 os.path.exists(os.path.join(
                     self.admin_repo_root,
                     "{}ValidRepository.kt".format(str(self.name).capitalize()))
                 )) and not self.force and self.integrate)
        ):
            print("repo dir is invalid or target file already exists")
            exit(13)

        self.admin_entity_root = os.path.join(
            self.admin_root,
            "src/main/kotlin/ai/maum/api/admin/infra/entity"
        )

        # check admin entity dir
        if (not (os.path.exists(self.admin_entity_root)
                 and os.path.isdir(self.admin_entity_root)) or
                ((os.path.exists(os.path.join(
                    self.admin_entity_root,
                    "{}ModelEntity.kt".format(str(self.name).capitalize()))
                ) or
                 os.path.exists(os.path.join(
                     self.admin_entity_root,
                     "{}ValidEntity.kt".format(str(self.name).capitalize()))
                 )) and not self.force and self.integrate)
        ):
            print("entity dir is invalid")
            exit(13)

        self.temp_dir_name = "temp-admin-{}".format(self.name)

        self.param_list = []
        try:
            if args.param_json:
                try:
                    with open(args.param_json, "r") as f:
                        try:
                            self.param_list = json.load(f)["model_params"]
                        except KeyError as e:
                            print("{}: {}".format(type(e), e))
                    if not self.param_list:
                        print("warn: could not load parameter info from {}".format(args.param_json))
                        self.param_list = []
                except json.decoder.JSONDecodeError:
                    print("json decode error")
                    exit(-1)
        except OSError as e:
            if e.errno == 2:
                print("warn: no params were specified")
                self.param_list = []

    def run(self):
        # check temp dir
        if os.path.exists(self.temp_dir_name):
            if self.force:
                shutil.rmtree(self.temp_dir_name)
            else:
                print("temp path exists: {}".format(self.temp_dir_name))
                exit(-1)

        shutil.copytree("maum-api-admin-template", self.temp_dir_name)
        self.rename_files()
        self.rewrite_class_lines()
        self.add_model_params()
        if self.integrate:
            self.move_to_admin()
            self.add_enum()
            self.handler_fix()
            self.add_exchange()
            self.mopup()

    def rename_files(self):
        filename_header = str(self.name).capitalize()
        for file in self.get_filenames(self.temp_dir_name):
            file_new = file.replace("Template", filename_header)
            os.rename(file, file_new)

    def rewrite_class_lines(self):
        for file in self.get_filenames(self.temp_dir_name, extension="kt"):
            new_file_lines = []
            with open(file, "r") as file_p:
                for line in file_p.readlines():
                    new_file_lines.append(
                        line.replace("template", self.name)
                            .replace("Template", str(self.name).capitalize())
                            .replace("TEMPLATE", str(self.name).upper())
                    )
            with open(file, "w") as file_p:
                file_p.writelines(new_file_lines)

    def add_model_params(self):
        model_file = os.path.join(self.temp_dir_name, "entity/{}ModelEntity.kt".format(str(self.name).capitalize()))
        model_lines = []
        with open(model_file, "r") as f:
            for line in f.readlines():
                if "generate_point" in line:
                    line_column = "    @Column\n"
                    to_string_ret_line = "        return \"{}ModelEntity(" \
                                         "id=$id, " \
                                         "modelName='$modelName', " \
                                         "host='$host', " \
                                         "port=$port, " \
                                         "open=$open".format(str(self.name).capitalize())

                    for param in self.param_list:
                        line_param = "    var {}: {}".format(param["name"], param["type"])
                        if param["type"] == "String" and param["default"] is not None:
                            line_param += " = \"{}\"".format(param["default"])
                            to_string_ret_line += ", {}='${}'".format(param["name"], param["name"])
                        else:
                            line_param += " = {}".format(param["default"])
                            to_string_ret_line += ", {}=${}".format(param["name"], param["name"])
                        line_param += "\n\n"
                        model_lines.extend([line_column, line_param])

                    to_string_ret_line += ")\"\n"
                    to_string_template = [
                        "    override fun toString(): String {\n",
                        to_string_ret_line,
                        "    }\n"
                    ]
                    model_lines.extend(to_string_template)
                else:
                    model_lines.append(line)
        with open(model_file, "w") as f:
            f.writelines(model_lines)

    def move_to_admin(self):
        for entity_file in self.get_filenames(
                os.path.join(self.temp_dir_name, "entity"),
                extension="kt"
        ):
            os.rename(
                entity_file,
                os.path.join(self.admin_entity_root, os.path.basename(entity_file))
            )

        for repo_file in self.get_filenames(
                os.path.join(self.temp_dir_name, "repository"),
                extension="kt"
        ):
            os.rename(
                repo_file,
                os.path.join(self.admin_repo_root, os.path.basename(repo_file))
            )

    def add_enum(self):
        try:
            engine_enum_lines = []
            with open(os.path.join(
                self.admin_root,
                "src/main/kotlin/ai/maum/api/admin/utils/enum/Engines.kt",
            ), "r") as f:
                for line in f.readlines():
                    if ";" in line:
                        new_line = str(line[:-2])
                        new_line += ",\n"
                        engine_enum_lines.append(new_line)
                        template_line = "    {}(\"{}\", listOf(".format(
                            str(self.name).upper(),
                            self.name
                        )
                        for param in self.param_list:
                            template_line += "\"{}\", ".format(param["name"])
                        if template_line.endswith(", "):
                            template_line = template_line[:-2]
                        template_line += "));\n"
                        engine_enum_lines.append(template_line)
                    else:
                        engine_enum_lines.append(line)

            with open(os.path.join(
                self.admin_root,
                "src/main/kotlin/ai/maum/api/admin/utils/enum/Engines.kt"
            ), "w") as f:
                f.writelines(engine_enum_lines)

        except OSError as e:
            if e.errno == 2:
                print("warn: could not find 'ai.maum.api.admin.utils.enum.Engines")
            else:
                raise e

    def handler_fix(self):
        try:
            entity_handler_lines = []
            with open(os.path.join(
                    self.admin_root,
                    "src/main/kotlin/ai/maum/api/admin/utils/reflect/EntityHandler.kt"), "r") as f:
                for line in f.readlines():
                    if "else" in line:
                        template_line = ""
                        last_line = entity_handler_lines[-1]
                        if "Model" in last_line:
                            template_line = "            Engines.{} -> {}ModelEntity()\n".format(
                                str(self.name).upper(),
                                str(self.name).capitalize()
                            )
                        elif "Valid" in last_line:
                            template_line = "            Engines.{} -> {}ValidEntity()\n".format(
                                str(self.name).upper(),
                                str(self.name).capitalize()
                            )
                        entity_handler_lines.append(template_line)
                    entity_handler_lines.append(line)

            with open(os.path.join(
                    self.admin_root,
                    "src/main/kotlin/ai/maum/api/admin/utils/reflect/EntityHandler.kt"), "w") as f:
                f.writelines(entity_handler_lines)
        except OSError as e:
            if e.errno == 2:
                print("warn: could not find 'ai.maum.api.admin.utils.reflect.EntityHandler")
            else:
                raise e

        try:
            repo_handler_lines = []
            with open(os.path.join(
                    self.admin_root,
                    "src/main/kotlin/ai/maum/api/admin/utils/reflect/RepositoryHandler.kt"), "r") as f:
                for line in f.readlines():
                    if "){" in line and "private" in repo_handler_lines[-1]:
                        repo_handler_lines.append("        private val {}ModelRepository: {}ModelRepository,\n".format(
                            self.name,
                            str(self.name).capitalize()
                        ))
                        repo_handler_lines.append("        private val {}ValidRepository: {}ValidRepository,\n".format(
                            self.name,
                            str(self.name).capitalize()
                        ))
                    if "else" in line:
                        template_line = ""
                        last_line = repo_handler_lines[-1]
                        if "Model" in last_line:
                            template_line = "            Engines.{} -> {}ModelRepository\n".format(
                                str(self.name).upper(),
                                self.name
                            )
                        elif "Valid" in last_line:
                            template_line = "            Engines.{} -> {}ValidRepository\n".format(
                                str(self.name).upper(),
                                self.name
                            )
                        repo_handler_lines.append(template_line)
                    repo_handler_lines.append(line)
            with open(os.path.join(
                    self.admin_root,
                    "src/main/kotlin/ai/maum/api/admin/utils/reflect/RepositoryHandler.kt"), "w") as f:
                f.writelines(repo_handler_lines)
        except OSError as e:
            if e.errno == 2:
                print("warn: could not find 'ai.maum.api.admin.utils.reflect.RepositoryHandler")
            else:
                raise e

    def add_exchange(self):
        try:
            print(self.admin_root)
            common_yml_lines = []
            with open(os.path.join(
                self.admin_root,
                "src/main/resources/application-common.yml"
            )) as f:
                common_yml_lines = f.readlines()
            common_yml_lines.append("      - {}:{}-model-update\n".format(self.name, self.name))
            with open(os.path.join(
                self.admin_root,
                    "src/main/resources/application-common.yml"
            ), "w") as f:
                f.writelines(common_yml_lines)
        except OSError as e:
            if e.errno == 2:
                print("warn: could not find application-common.yml")
            else:
                raise e

    def mopup(self):
        shutil.rmtree(self.temp_dir_name)

    @staticmethod
    def get_filenames(root_dir: str, extension: str = None, basename: bool = False):
        filenames = []

        for dir_, _, files in os.walk(root_dir):
            for file in files:
                if (extension is None) or (file.endswith(extension)):
                    if basename:
                        filenames.append(os.path.basename(os.path.join(dir_, file)))
                    else:
                        filenames.append(os.path.join(dir_, file))

        return filenames
