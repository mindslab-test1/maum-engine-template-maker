import os
import argparse

from template_maker import TemplateMaker as Writer


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--name", help="Engine Name", default="test", type=str)
    parser.add_argument("-d", "--destination", help="destination dir", default="{}".format(os.environ["HOME"]), type=str)
    parser.add_argument("-m", "--maintainer", help="maintainer", default="aquashdw", type=str)
    parser.add_argument("-p", "--param_json", help="additional model parameters to be added", default="param.json", type=str)
    parser.add_argument("-f", "--force", help="remove existing files", default=False, type=bool)
    parser.add_argument("--port", help="new server port", default=8080, type=int)

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    maker = Writer(args)
    maker.run()
