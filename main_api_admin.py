import os
import argparse

from admin_maker import AdminMaker as maker


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--name", help="Engine Name", default="test", type=str)
    parser.add_argument("--admin_dir", help="maum-api-admin project root dir", default="{}/git/maum-api-admin".format(os.environ["HOME"]), type=str)
    parser.add_argument("-p", "--param_json", help="additional model parameters to be added", default="param.json", type=str)
    parser.add_argument("-f", "--force", help="remove existing files", default=False, type=bool)
    parser.add_argument("-i", "--integrate", help="integrate to admin project", default=False, type=bool)

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    maker = maker(args)
    maker.run()
