package ai.maum.api.admin.infra.entity

import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@DynamicUpdate
@Table(name = "template_model_info")
class TemplateModelEntity : BaseEntity() {
    @Id
    @SequenceGenerator(name = "TEMPLATE_MODEL_SEQ_GEN", sequenceName = "TEMPLATE_MODEL_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEMPLATE_MODEL_SEQ_GEN")
    var id: Long? = null

    @Column
    var modelName: String = ""

    @Column
    var host: String = ""

    @Column
    var port: Long = 0

    @Column
    var open: Boolean = false

    // generate_point
}