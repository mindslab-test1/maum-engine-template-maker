package ai.maum.api.admin.infra.repository

import ai.maum.api.admin.infra.entity.TemplateModelEntity
import org.springframework.data.repository.CrudRepository

interface TemplateModelRepository: CrudRepository<TemplateModelEntity, Long>, ModelRepository {
    override fun findByModelNameAndActiveIsTrue(modelName: String): TemplateModelEntity?
    override fun findAllByActiveIsTrue(): MutableList<TemplateModelEntity>?

    override fun existsByModelNameAndActiveIsTrue(modelName: String): Boolean
}