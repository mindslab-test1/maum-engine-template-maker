package ai.maum.api.admin.infra.repository

import ai.maum.api.admin.infra.entity.UserEntity
import ai.maum.api.admin.infra.entity.TemplateValidEntity
import org.springframework.data.repository.CrudRepository

interface TemplateValidRepository: CrudRepository<TemplateValidEntity, Long>, ValidRepository {
    override fun findByClientAndModel(client: UserEntity, model: Any): TemplateValidEntity?
    override fun findByClientAndModelAndActiveIsTrue(client: UserEntity, model: Any): TemplateValidEntity?
    override fun findAllByModelAndActiveIsTrue(modelEntity: Any): MutableList<TemplateValidEntity>?
}