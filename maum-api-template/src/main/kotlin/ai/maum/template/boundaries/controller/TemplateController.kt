package ai.maum.template.boundaries.controller

import ai.maum.template.core.usecase.TemplateUsecase
import org.slf4j.LoggerFactory
import org.springframework.security.oauth2.jwt.JwtDecoder
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/template")
class TemplateController (
        val templateUsecase: TemplateUsecase,
        private val jwtDecoder: JwtDecoder
){
    private val logger = LoggerFactory.getLogger(this.javaClass)

    // TODO create endpoints for API

    private fun extractClientId(oAuth2Authentication: OAuth2Authentication): String{
        val tokenValue = (oAuth2Authentication.details as OAuth2AuthenticationDetails).tokenValue
        val jwtMap = jwtDecoder.decode(tokenValue)
        return jwtMap.getClaim("app_id")
    }
}