package ai.maum.template.boundaries.dto

data class TemplateRequestDto(
        val model: String?
)