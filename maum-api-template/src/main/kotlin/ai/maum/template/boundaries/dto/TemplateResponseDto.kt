package ai.maum.template.boundaries.dto

data class TemplateResponseDto(
        val status: Int? = 0,
        val message: String? = "Success",
        val payload: Any? = null
)
