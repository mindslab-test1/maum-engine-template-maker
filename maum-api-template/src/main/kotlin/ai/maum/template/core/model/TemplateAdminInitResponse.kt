package ai.maum.template.core.model

import kotlinx.serialization.Serializable

@Serializable
data class TemplateAdminInitResponse(
        val status: Int? = 0,
        val message: String? = "",
        val payload: TemplateInitData = TemplateInitData()
)

@Serializable
data class TemplateInitData(
        val modelList: List<TemplateModelWithUsers> = mutableListOf()
)

@Serializable
data class TemplateModelWithUsers(
        val model: TemplateModel,
        val modelName: String,
        val users: Set<String>
)