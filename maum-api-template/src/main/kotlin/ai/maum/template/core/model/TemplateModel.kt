package ai.maum.template.core.model

import kotlinx.serialization.Serializable

@Serializable
data class TemplateModel(
        val host: String,
        val port: Int,
        val open: Boolean? = false,
        // generate_point
)