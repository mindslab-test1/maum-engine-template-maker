package ai.maum.template.core.model

interface TemplateModelRepository {
    fun getModelInfo(clientId: String, model: String): TemplateModel
    fun update(templateModelUpdate: TemplateModelUpdate)
    fun testGetModels(): MutableMap<String, Pair<TemplateModel, MutableSet<String>>>
}