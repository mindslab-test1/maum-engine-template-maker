package ai.maum.template.core.model

import kotlinx.serialization.Serializable

@Serializable
data class TemplateModelUpdate(
        val action: String? = "none",
        val modelName: String? = "",
        val modelInfo: TemplateModel? = null,
        val clientAction: String? = "none",
        val client: String? = null
)