package ai.maum.template.core.service

import org.springframework.web.multipart.MultipartFile

interface MediaService {
    fun saveContent(file: MultipartFile)
}