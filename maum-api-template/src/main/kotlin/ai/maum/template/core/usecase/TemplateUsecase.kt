package ai.maum.template.core.usecase

import ai.maum.template.core.model.TemplateModelRepository
import ai.maum.template.core.service.TemplateService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class TemplateUsecase(
        val templateModelRepository: TemplateModelRepository,
        val templateService: TemplateService
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    // TODO create validation for APIs and proceed to business logic
}