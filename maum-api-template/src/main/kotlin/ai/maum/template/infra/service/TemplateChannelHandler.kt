package ai.maum.template.infra.service

import io.grpc.ManagedChannel

interface TemplateChannelHandler {
    fun getOrPutChannel(modelName: String, host: String, port: Int): ManagedChannel
    fun removeChannel(modelName: String?): Unit
}