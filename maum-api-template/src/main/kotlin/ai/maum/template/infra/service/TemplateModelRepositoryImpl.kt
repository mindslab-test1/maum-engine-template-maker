package ai.maum.template.infra.service

import ai.maum.template.core.exception.*
import ai.maum.template.core.model.TemplateModel
import ai.maum.template.core.model.TemplateModelRepository
import ai.maum.template.core.model.TemplateModelUpdate
import ai.maum.template.core.model.TemplateAdminInitResponse
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.server.ResponseStatusException

@Service
class TemplateModelRepositoryImpl(
        adminWebClient: WebClient,
        private val templateChannelHandler: TemplateChannelHandler
): TemplateModelRepository {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    private val memoryMap = mutableMapOf<String, Pair<TemplateModel, MutableSet<String>>>()

    init {
        val initRequest = adminWebClient
                .get()
                .uri("/admin/api/init/getInitialData")
                .header("ai-maum-engine", "TEMPLATE")
                .retrieve()

        val responseRaw = initRequest.bodyToMono(String::class.java)
                .block() ?: throw ApiTransactionalException(
                message = "response from admin is null!"
        )
        logger.debug("${Json.decodeFromString<TemplateAdminInitResponse>(responseRaw)}")

        val responsePayload = Json.decodeFromString<TemplateAdminInitResponse>(responseRaw).payload
        responsePayload.modelList.forEach { modelWithUsers ->
            val modelName = modelWithUsers.modelName
            val modelInfo = TemplateModel(
                    host = modelWithUsers.model.host,
                    port = modelWithUsers.model.port,
                    open = modelWithUsers.model.open,
                    // generate_point
            )
            val clientSet = modelWithUsers.users as MutableSet<String>
            memoryMap[modelName] = Pair(first = modelInfo, second = clientSet)
        }

        logger.info("loaded model list from admin server")
        for(mutableEntry in memoryMap) {
            logger.debug(mutableEntry.key)
            logger.debug(mutableEntry.value.toString())
        }
    }

    override fun getModelInfo(clientId: String, model: String): TemplateModel {
        val modelPair = memoryMap[model] ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "requested model is not present")
        if(!(modelPair.first.open == true || modelPair.second.contains(clientId)))
            throw  ResponseStatusException(HttpStatus.FORBIDDEN, "model not authenticated for client")

        return modelPair.first
    }

    override fun update(templateModelUpdate: TemplateModelUpdate) {
        when(templateModelUpdate.action){
            "create" -> {
                createAction(templateModelUpdate)
            }
            "update" -> {
                updateAction(templateModelUpdate)
            }
            "delete" -> {
                val removed = memoryMap.remove(templateModelUpdate.modelName)
                logger.info("removed model: ${templateModelUpdate.modelName}")
                logger.debug("model info: $removed")
                templateChannelHandler.removeChannel(templateModelUpdate.modelName)
            }
            "none" -> {
                logger.warn("no action message presented")
            }
            else -> throw MqUnknownActionException(
                    message = "unknown action ${templateModelUpdate.action}",
                    mqObject = templateModelUpdate
            )
        }
    }

    override fun testGetModels(): MutableMap<String, Pair<TemplateModel, MutableSet<String>>> {
        return this.memoryMap
    }

    private fun createAction(templateModelUpdate: TemplateModelUpdate){
        templateModelUpdate.modelInfo ?: throw MqInsufficientDataException(
                message = "model info not specified in message",
                mqObject = templateModelUpdate
        )
        templateModelUpdate.client ?: throw MqInsufficientDataException(
                message = "model creator(default user) not specified in message",
                mqObject = templateModelUpdate
        )

        val pair = Pair(
                first = templateModelUpdate.modelInfo,
                second = mutableSetOf(templateModelUpdate.client)
        )
        memoryMap[templateModelUpdate.modelName ?: throw MqConCurrencyException(
                message = "null model name presented"
        )] = pair
        logger.info("created model: ${templateModelUpdate.modelName}")
        logger.debug("model info: ${templateModelUpdate.modelInfo}")

    }

    private fun updateAction(templateModelUpdate: TemplateModelUpdate){
        memoryMap[templateModelUpdate.modelName] ?: throw MqConCurrencyException(
                message = "concurrency exception: no known model name '${templateModelUpdate.modelName}', " +
                        "known models: ${memoryMap.keys}"
        )

        val clientSet: MutableSet<String> = LinkedHashSet()
        clientSet.addAll(memoryMap[templateModelUpdate.modelName]?.second ?: throw ApiInternalException(
                message = "could not find client set for model: ${templateModelUpdate.modelName}"
        ))
        val originModel = memoryMap[templateModelUpdate.modelName]?.first ?: throw ApiInternalException(
                message = "could not find origin model info: ${templateModelUpdate.modelName}"
        )

        when (templateModelUpdate.clientAction){
            "add" -> clientSet.add(templateModelUpdate.client ?: throw MqInsufficientDataException(
                    message = "client info not specified in message"
            ))
            "remove" -> clientSet.remove(templateModelUpdate.client ?: throw MqInsufficientDataException(
                    message = "client info not specified in message"
            ))
        }

        memoryMap[templateModelUpdate.modelName ?: throw MqConCurrencyException(
                message = "concurrency exception: no known model name '${templateModelUpdate.modelName}', " +
                        "known models: ${memoryMap.keys}"
        )] = Pair(
                first = templateModelUpdate.modelInfo ?: originModel,
                second = clientSet
        )

        logger.info("updated model: ${templateModelUpdate.modelName}")
        logger.debug("model info: ${templateModelUpdate.modelInfo}")
        logger.debug("client action: ${templateModelUpdate.clientAction}, client: ${templateModelUpdate.client}")

        templateChannelHandler.removeChannel(templateModelUpdate.modelName)
    }
}