package ai.maum.template.infra.service

import ai.maum.template.core.service.TemplateService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class TemplateServiceImpl(
        val templateChannelHandler: TemplateChannelHandler
): TemplateService {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    // TODO actually send request to engine GRPC
}