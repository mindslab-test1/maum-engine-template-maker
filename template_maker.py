import os
import json
import shutil


class TemplateMaker:
    def __init__(self, args):
        self.name = str(args.name).lower()
        self.destination = args.destination
        self.maintainer = args.maintainer
        self.param_list = []
        self.port = args.port
        if args.param_json:
            with open(args.param_json, "r") as f:
                self.param_list = json.load(f)["model_params"]
            if not self.param_list:
                self.param_list = []
        self.force = args.force
        self.new_project_name = "maum-api-{}".format(self.name)
        self.new_project_path = os.path.join(self.destination, self.new_project_name)

    def run(self):
        if os.path.exists(self.new_project_path):
            if self.force:
                shutil.rmtree(self.new_project_path)
            else:
                print("path exists: {}".format(self.new_project_path))
                exit(-1)

        shutil.copytree("maum-api-template", self.new_project_path)
        self.rename_files()
        self.rename_physical_dir()
        self.rewrite_class_lines()
        self.rewrite_script_lines()
        self.add_model_params()
        self.rewrite_profile_ymls()

    def rename_files(self):
        filename_header = str(self.name).capitalize()
        for file in self.get_filenames(self.new_project_path):
            file_new = file.replace("Template", filename_header)
            os.rename(file, file_new)

    def rename_physical_dir(self):
        os.rename(
            os.path.join(self.new_project_path, "src/main/kotlin/ai/maum/template"),
            os.path.join(self.new_project_path, "src/main/kotlin/ai/maum/{}".format(self.name))
        )
        os.rename(
            os.path.join(self.new_project_path, "src/test/kotlin/ai/maum/template"),
            os.path.join(self.new_project_path, "src/test/kotlin/ai/maum/{}".format(self.name))
        )

    def rewrite_class_lines(self):
        for file in self.get_filenames(self.new_project_path, extension="kt"):
            new_file_lines = []
            with open(file, "r") as file_p:
                for line in file_p.readlines():
                    new_file_lines.append(
                        line.replace("template", self.name)
                            .replace("Template", str(self.name).capitalize())
                            .replace("TEMPLATE", str(self.name).upper())
                    )
            with open(file, "w") as file_p:
                file_p.writelines(new_file_lines)

        for file in self.get_filenames(self.new_project_path, extension="yml"):
            new_file_lines = []
            with open(file, "r") as file_p:
                for line in file_p.readlines():
                    new_file_lines.append(
                        line.replace("template", self.name)
                            .replace("Template", str(self.name).capitalize())
                            .replace("TEMPLATE", str(self.name).upper())
                    )
            with open(file, "w") as file_p:
                file_p.writelines(new_file_lines)

        model_repo_lines = []
        with open(os.path.join(
                self.new_project_path,
                "src/main/kotlin/ai/maum/{}/infra/service/{}ModelRepositoryImpl.kt"
                        .format(self.name, str(self.name).capitalize())
        )) as f:
            for line in f.readlines():
                if "generate_point" in line:
                    for param in self.param_list:
                        line_param = "                    {} = modelWithUsers.model.{},\n".format(
                            param["name"], param["name"]
                        )
                        model_repo_lines.append(line_param)
                else:
                    model_repo_lines.append(line)

        with open(os.path.join(
                self.new_project_path,
                "src/main/kotlin/ai/maum/{}/infra/service/{}ModelRepositoryImpl.kt"
                        .format(self.name, str(self.name).capitalize())),
                "w") as f:
            f.writelines(model_repo_lines)

    def rewrite_script_lines(self):
        new_build_script_lines = []
        with open(os.path.join(self.new_project_path, "build.gradle.kts"), "r") as gradle_pointer:
            for line in gradle_pointer.readlines():
                new_build_script_lines.append(
                    line.replace("template", self.name)
                )
        with open(os.path.join(self.new_project_path, "build.gradle.kts"), "w") as gradle_pointer:
            gradle_pointer.writelines(new_build_script_lines)

        new_setting_script_lines = []
        with open(os.path.join(self.new_project_path, "settings.gradle.kts"), "r") as gradle_pointer:
            for line in gradle_pointer.readlines():
                new_setting_script_lines.append(
                    line.replace("template", self.name)
                )
        with open(os.path.join(self.new_project_path, "settings.gradle.kts"), "w") as gradle_pointer:
            gradle_pointer.writelines(new_setting_script_lines)

        new_dockerfile_lines = []
        with open(os.path.join(self.new_project_path, "Dockerfile"), "r") as dockerfile_pointer:
            for line in dockerfile_pointer.readlines():
                new_dockerfile_lines.append(
                    line.replace("maintainer", self.maintainer)
                        .replace("template", self.name)
                        .replace("Template", str(self.name).capitalize())
                )
        with open(os.path.join(self.new_project_path, "Dockerfile"), "w") as dockerfile_pointer:
            dockerfile_pointer.writelines(new_dockerfile_lines)

    def rewrite_profile_ymls(self):
        common_yml_path = os.path.join(self.new_project_path, "src/main/resources/application-common.yml")
        new_lines = []
        with open(common_yml_path, "r") as f:
            for line in f.readlines():
                if "port" in line:
                    new_lines.append("  port: {}".format(self.port))
                else:
                    new_lines.append(line)
        with open(common_yml_path, "w") as f:
            f.writelines(new_lines)

    def add_model_params(self):
        model_file = os.path.join(
            self.new_project_path,
            "src/main/kotlin/ai/maum/{}/core/model/{}Model.kt".format(self.name, self.name.capitalize())
        )
        model_lines = []
        with open(model_file, "r") as f:
            for line in f.readlines():
                if "generate_point" in line:
                    for param in self.param_list:
                        line_param = "        val {}: {}".format(param["name"], param["type"])
                        if "default" in param.keys():
                            line_param += " = "
                            if param["type"] == "String" and param["default"] is not None:
                                line_param += "\"{}\"".format(param["default"])
                            else:
                                line_param += "{}".format(param["default"])
                        line_param += ",\n"
                        model_lines.append(line_param)
                else:
                    model_lines.append(line)
        with open(model_file, "w") as f:
            f.writelines(model_lines)

    @staticmethod
    def get_filenames(root_dir: str, extension: str = None, basename: bool = False):
        filenames = []

        for dir_, _, files in os.walk(root_dir):
            for file in files:
                if (extension is None) or (file.endswith(extension)):
                    if basename:
                        filenames.append(os.path.basename(os.path.join(dir_, file)))
                    else:
                        filenames.append(os.path.join(dir_, file))

        return filenames

